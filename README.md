# stuart-challenge

This project needs a `.env` file with a valid Google Maps API key
```
GOOGLE_MAPS_API_KEY=
```

## TODO:

0. MISSEMPLACE:

- [x] check https://stuart­frontend­challenge.vercel.app
- [x] check graphql https://stuart­frontend­challenge.vercel.a/graphql
- [x] research about webpack and babel manually
- [x] research about Google Maps API

1. Implement the design ✅

2. Add geocode address when user leaves the field ✅

   - This can be done by sending a request to POST

         https://stuart­frontend­challenge.vercel.app/geoco
         { "address": "29 Rue du 4 Septembre" }

     If the address is valid, the icon color should change (yellow for pick up
     and green for drop off) and a marker should be added on the map.

   - You also have an option to use our grapqhl API to produce the same result
     as above. You can explore the provided schema at

         https://stuart­frontend­challenge.vercel.app/graphql ✅

3. If the address is invalid, the icon color should be red. ✅

4. Once the form is filled with two valid addresses, the button should be
   enabled.

5. When the user clicks on the button, a request should be sent to POST

       https://stuart­frontendchallenge.vercel.app/jobs with
       { "pickup": "29 Rue du 4 Septembre", "dropoff": "15 Rue de Bourgogne" }

   - or with graphql:

         https://stuart­frontend­challenge.vercel.app/graphql

6. Once the POST **https://stuart­frontend­challenge.vercel.app/jobs** request is
   done, the form should be reseted and a toaster saying "Job has been created
   successfully!" should appear. If the user clicks, the toaster should
   disappear.

7. How would you improve the app? Write in a few lines how do you think the app
   can be improved and what would you do different if you had more time.

## Bonus points

1. Set up webpack and babel manually. ✅
2. Use Google Maps API without any libs. ✅
3. Geocode the address on blur. ✅ and once the user stops typing ❌
4. While the request is pending, the button should be disabled and its wording
   should change to "Creating...".
5. Make the toaster disappear in 5 secs.

## NOTES:

- The REST API is deployed at **https://stuart­frontend­challenge.vercel.app**

- The GraphQL API is deployed at

      https://stuart­frontend­challenge.vercel.app/graphql

- The API (both REST and GraphQL) work with two addresses. If the API does not
  know an address it will return an error. 

      29 Rue du 4 Septembre and 15 Rue de Bourgogne"

- Assets can be found in the assets/ folder.

- Roboto can be downloaded on Google Fonts.

- Use Google Maps Javascript API.

- App must work in the latest Chrome version.

- Source code must be hosted on git (You can create a free repository in Github,
  Gitlab or Bitbucket).

- Once you are done, send us an email with: a link to the git repository, the
  time spent on the project and a list of any issues you encountered while doing
  the challenge.

- The challenge should take anything from 3 to 4 hours without the bonus points.
  It's ok to turn it in unfinished if you don't want to spend more than the
  recommended time, we don't want the process to be too long. On average,
  candidates take from 6 to 8 hours with the bonus points.

- Please contact us if you have any question.

## Resources 

- Webpack setup: https://medium.com/swlh/2020-settings-of-react-typescript-project-with-webpack-and-babel-403c92feaa06
