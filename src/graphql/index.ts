import { gql, GraphQLClient } from 'graphql-request';
import { useQuery } from 'react-query';

const API_URL = 'https://stuart-frontend-challenge.vercel.app/graphql';

const graphQLClient = new GraphQLClient(API_URL);

const GEO_QUERY = gql`
    query ($address: String!) {
        geocode(address: $address) {
            latitude
            longitude
        }
    }
`;

export function useGetGeolocation(address: string) {
    return useQuery(
        ['get-geo', address],
        async () => {
            const { geocode } = await graphQLClient.request(GEO_QUERY, {
                address
            });
            return geocode;
        },
        {
            refetchOnWindowFocus: false,
            enabled: false,
            retry: false
        }
    );
}
