import React from 'react';
import { SearchBox } from '../SearchBox';
import './index.scss';

export const AddressModal: React.FC<{}> = () => {
    return (
        <div className="address">
            <SearchBox placeholder="Pick up address" type="pickUp" />
            <SearchBox placeholder="Drop off address" type="dropOff" />
            <button>Create Job</button>
        </div>
    );
};
