import React from 'react';
import './index.scss';

import dropOffBadgeBlank from '../../assets/dropOffBadgeBlank.svg';
import dropOffBadgeError from '../../assets/dropOffBadgeError.svg';
import dropOffBadgePresent from '../../assets/dropOffBadgePresent.svg';
import dropOffMarker from '../../assets/dropOffMarker.svg';
import pickUpBadgeBlank from '../../assets/pickUpBadgeBlank.svg';
import pickUpBadgeError from '../../assets/pickUpBadgeError.svg';
import pickUpBadgePresent from '../../assets/pickUpBadgePresent.svg';
import pickUpMarker from '../../assets/pickUpMarker.svg';

type MarkerProps = {
    type: string;
    isError: boolean;
    isSuccess: boolean;
    resetMarker: boolean;
};

type M = {
    [name: string]: string;
};

const generateMarkerType = ({
    type,
    isError,
    isSuccess,
    resetMarker
}: MarkerProps) => {
    if (resetMarker) {
        return `${type}BadgeBlank`;
    } else if (isSuccess) {
        return `${type}BadgePresent`;
    } else if (isError) {
        return `${type}BadgeError`;
    } else {
        return `${type}BadgeBlank`;
    }
};

export const Marker: React.FC<MarkerProps> = ({
    type,
    isError,
    isSuccess,
    resetMarker
}: MarkerProps) => {
    const markerType = generateMarkerType({
        type,
        isError,
        isSuccess,
        resetMarker
    });

    const MARKERS: M = {
        dropOffBadgeBlank,
        dropOffBadgePresent,
        dropOffBadgeError,
        dropOffMarker,
        pickUpBadgeBlank,
        pickUpBadgeError,
        pickUpBadgePresent,
        pickUpMarker
    };

    return (
        <div className="marker">
            <img src={MARKERS[markerType]} alt={type} />
        </div>
    );
};
