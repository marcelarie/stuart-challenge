import React, { useContext, useEffect, useRef, useState } from 'react';
import { AppContext, MarkerDataT } from '../../App';
import { useDispatchMarker } from '../../hooks/useDispatchMarker';
import './index.scss';

const initMap = (
    ref: React.RefObject<HTMLDivElement>
): google.maps.Map | undefined => {
    const defaultCoordinates = { lat: 48.864716, lng: 2.349014 };

    if (ref.current) {
        const map = new google.maps.Map(ref.current as HTMLElement, {
            center: defaultCoordinates,
            zoom: 10,
            disableDefaultUI: true
        });

        return map;
    }
};

const createMarker = (
    markerData: MarkerDataT,
    map: google.maps.Map | undefined,
    title: string
) => {
    const position = new google.maps.LatLng(markerData.lat, markerData.lon);

    return new google.maps.Marker({
        position,
        map,
        title
    });
};

export const Map: React.FC<{}> = () => {
    const mapRef = useRef<HTMLDivElement>(null);
    const [map, setMap] = useState<google.maps.Map>();
    const { markers, setMarkers } = useContext(AppContext);

    useEffect(() => {
        if (!map) {
            setMap(initMap(mapRef));
        }
    }, [map]);

    // TODO: encapsulate all this logic in the customHook
    useEffect(() => {
        if (map) {
            if (markers.pickUp?.enabled && !markers.pickUp.marker) {
                useDispatchMarker(
                    createMarker(markers.pickUp, map, 'pickUp'),
                    markers,
                    setMarkers
                );
            } else if (markers.dropOff?.enabled && !markers.dropOff.marker) {
                useDispatchMarker(
                    createMarker(markers.dropOff, map, 'dropOff'),
                    markers,
                    setMarkers
                );
            }
        }
    }, [markers]);

    return (
        <div className="map_container">
            <div ref={mapRef} id="map" />
        </div>
    );
};
