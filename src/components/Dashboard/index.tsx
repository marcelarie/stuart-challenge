import React from 'react';
import { useScript } from '../../hooks/useScript';

import { AddressModal } from '../AddressModal';
import { Map } from '../Map';

import './index.scss';

export const Dashboard: React.FC<{}> = () => {
    const GOOGLE_SCRIPT_URL = `https://maps.googleapis.com/maps/api/js?key=${process.env.GOOGLE_MAPS_API_KEY}&callback=initMap`;
    const mapIsLoaded = useScript(GOOGLE_SCRIPT_URL);

    return (
        <div className="dashboard">
            {mapIsLoaded && (
                <>
                    <AddressModal />
                    <Map />
                </>
            )}
        </div>
    );
};
