import React, { useContext, useEffect, useState } from 'react';
import { useGetGeolocation } from '../../graphql';
import { AppContext, MarkerT } from '../../App';
import { Marker } from '../Marker';
import './index.scss';

type InputProps = {
    placeholder: string;
    type: string;
};

const resetGoogleMarkers = (
    markers: MarkerT,
    setMarkers: React.Dispatch<React.SetStateAction<MarkerT>> | undefined,
    type: string
) => {
    console.log('in');

    if (type === 'dropOff' && markers.dropOff) {
        markers.dropOff.marker?.setVisible(false);
        setMarkers &&
            setMarkers({
                ...markers,
                dropOff: {
                    ...markers.dropOff,
                    marker: undefined
                }
            });
    } else if (type === 'pickUp' && markers.pickUp) {
        console.log(markers);

        markers.pickUp.marker?.setMap(null);
        setMarkers &&
            setMarkers({
                ...markers,
                pickUp: {
                    ...markers.pickUp,
                    marker: undefined
                }
            });
    }
};

export const SearchBox: React.FC<InputProps> = ({
    placeholder,
    type
}: InputProps) => {
    const [value, setValue] = useState('');
    const [geoValue, setGeoValue] = useState('');
    const [resetMarker, setResetMarker] = useState(true);
    const { setMarkers, markers } = useContext(AppContext);

    const { refetch, isError, isSuccess, data } = useGetGeolocation(geoValue);

    useEffect(() => {
        if (data) {
            const markerData = {
                lat: data.latitude,
                lon: data.longitude,
                enabled: true // use isSuccess here (?)
            };

            if (type === 'dropOff') {
                setMarkers && setMarkers({ ...markers, dropOff: markerData });
            } else if (type === 'pickUp') {
                setMarkers && setMarkers({ ...markers, pickUp: markerData });
            }
        }
    }, [data]);

    const handleValue = (e: React.ChangeEvent<any>) => {
        setValue(e.target.value);
        if (e.target.value) setGeoValue(e.target.value);
    };

    const handleBlur = () => {
        if (value) {
            setResetMarker(false);
            refetch();
        } else {
            setResetMarker(true);
            resetGoogleMarkers(markers, setMarkers, type);
        }
    };

    return (
        <div className="address_field">
            <Marker
                type={type}
                isError={isError}
                isSuccess={isSuccess}
                resetMarker={resetMarker}
            />
            <input
                placeholder={placeholder}
                value={value}
                onChange={handleValue}
                onBlur={handleBlur}
            />
        </div>
    );
};

// TODO:
// - use onError and onSuccess react-query callback
// - toaster onError
// - Markers onSuccess
