// import { useContext, useEffect } from 'react';
import { MarkerDataT, MarkerT } from '../../App';

export const useDispatchMarker = (
    marker: google.maps.Marker,
    markers: MarkerT,
    setMarkers: React.Dispatch<React.SetStateAction<MarkerT>> | undefined
) => {
    if (marker.getTitle() === 'pickUp') {
        if (markers.pickUp?.enabled) {
            const pickUp: MarkerDataT = { ...markers.pickUp, marker };
            setMarkers && setMarkers({ ...markers, pickUp });
        }
    } else if (marker.getTitle() === 'dropOff') {
        if (markers.dropOff?.enabled) {
            const dropOff: MarkerDataT = { ...markers.dropOff, marker };
            setMarkers && setMarkers({ ...markers, dropOff });
        }
    }
};
