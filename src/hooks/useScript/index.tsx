import { useEffect, useState } from 'react';

export const useScript = (src: string) => {
    const [mapIsLoaded, setMapIsLoaded] = useState(false);

    useEffect(() => {
        const script = document.createElement('script');
        const initMap = document.createElement('script');
        initMap.type = 'text/javascript';
        initMap.text = 'function initMap() {}';
        script.src = src;
        script.async = true;
        script.defer = true;
        document.body.appendChild(initMap);
        document.body.appendChild(script);
        script.addEventListener('load', () => setMapIsLoaded(true));
    }, []);

    return mapIsLoaded;
};
