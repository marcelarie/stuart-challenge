import React, { useState } from 'react';
import { Dashboard } from './components/Dashboard';
import './styles/index.scss';

export type MarkerDataT = {
    lat: number;
    lon: number;
    enabled: boolean;
    marker?: google.maps.Marker;
};
export type MarkerT = {
    pickUp?: MarkerDataT;
    dropOff?: MarkerDataT;
};

type ContextT = {
    markers: MarkerT;
    setMarkers?: React.Dispatch<React.SetStateAction<MarkerT>>;
};

const context: ContextT = {
    markers: {
        pickUp: { lat: 0, lon: 0, enabled: false },
        dropOff: { lat: 0, lon: 0, enabled: false }
    }
};

export const AppContext: React.Context<ContextT> = React.createContext(context);

function App() {
    const [markers, setMarkers] = useState(context.markers);
    return (
        <>
            <AppContext.Provider value={{ markers, setMarkers }}>
                <Dashboard />
            </AppContext.Provider>
        </>
    );
}

export default App;
